use core::result::Result;
use std::error::Error;
use std::fs;
use colored::*;

/// Main parse execution entry point.
pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let content = fs::read_to_string(&config.filepath)?;
    let result = search(&config.query, &content);

    for line in result {
        println!("{}", line);
    }

    Ok(())
}

pub fn search(query: &str, content: &str) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();

    for (index, line) in content.lines().enumerate() {
        if line.contains(query) {
            let mut colored_line = line.to_string();
            let v: Vec<_>
                = colored_line.match_indices(query).map(|(i, _)|i).collect();

            let mut fmt_line: String = String::new();

            let mut seqs = colored_line.split(query).collect::<Vec<&str>>();
            let last_seq = seqs.pop().unwrap();

            for seq in seqs {
                fmt_line = format!("{}{}{}", fmt_line, seq, query.red().italic().bold());
            }

            fmt_line=format!("{}{}", fmt_line, last_seq);

            let a = format!("[{}]: {}", index+1, fmt_line);

            result.push(a);
        }
    }
    result
}

/// The parameter configuration data-type
pub struct Config {
    query: String,
    filepath: String,
}

impl Config {
    /// Empty Constructor
    pub fn new() -> Config {
        Config {
            query: String::new(),
            filepath: String::new(),
        }
    }

    /// Parameter constructor (takes the list of arguments as parameter)
    pub fn from(args: &[String]) -> Result<Config, &str> {
        if args.len() < 3 {
            return Err("The call doesn't have enough arguments.");
        }
        Ok(Config {
            query: args[1].clone(),
            filepath: args[2].clone(),
        })
    }
}

#[cfg(tests)]
mod tests{
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick Three.";
        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents);
        );
    }
}