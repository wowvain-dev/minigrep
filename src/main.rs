use minigrep::Config;

use std::env;

fn main() {
    let args = env::args().collect::<Vec<String>>();
    let config = Config::from(&args).unwrap_or_else(|err| {
        println!("{}", err);
        std::process::exit(1);
    });


    if let Err(e) = minigrep::run(config) {
        println!("Application error: {e}");
        std::process::exit(1);
    }
}
