# minigrep

This is a very minimal grep tool I've built to better understand some basic Rust string concepts (espacially string related operations).

It doesn't yet support folder paths, can only check a specific file.


### Installation

```
git clone git@gitlab.com:wowvain-dev/minigrep
cd minigrep
cargo run -- <QUERY> <FILEPATH>
```

### Usage without cargo

```
cargo build --release
cd target/release
./minigrep <QUERY> <FILEPATH> 
```
